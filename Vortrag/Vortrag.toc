\beamer@endinputifotherversion {3.36pt}
\select@language {german}
\beamer@sectionintoc {1}{Spannb\IeC {\"a}ume und Rucks\IeC {\"a}cke}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Problem des minimalen Spannbaums}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Rucksack-Problem}{9}{0}{1}
\beamer@sectionintoc {2}{Blockkoordinaten-Abstiegsverfahren}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Algorithmus}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Formulierungen mit Bl\IeC {\"o}cken}{30}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{Problem des minimalen Spannbaums}{31}{0}{2}
\beamer@subsubsectionintoc {2}{2}{2}{Rucksack-Problem}{68}{0}{2}
\beamer@subsectionintoc {2}{3}{Wahl der Bl\IeC {\"o}cke}{72}{0}{2}
\beamer@sectionintoc {3}{Ergebnisse}{129}{0}{3}
\beamer@sectionintoc {4}{Schluss}{168}{0}{4}
