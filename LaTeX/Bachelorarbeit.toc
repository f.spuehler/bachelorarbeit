\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Einleitung}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Spannb\IeC {\"a}ume und Rucks\IeC {\"a}cke}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Problem des minimalen Spannbaums}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Einf\IeC {\"u}hrung in die Graphentheorie}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Problemformulierung}{4}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Rucksack-Problem}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Problemformulierung}{7}{subsection.2.2.1}
\contentsline {chapter}{\numberline {3}Blockkoordinaten"=Abstiegsverfahren}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Der Algorithmus}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Problemformulierungen mit Bl\IeC {\"o}cken}{11}{section.3.2}
\contentsline {subsubsection}{\nonumberline Problem des minimalen Spannbaums}{11}{section*.4}
\contentsline {subsubsection}{\nonumberline Rucksack-Problem}{23}{section*.10}
\contentsline {section}{\numberline {3.3}Wahl der Bl\IeC {\"o}cke}{25}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Reihenfolge der Bl\IeC {\"o}cke}{26}{subsection.3.3.1}
\contentsline {subsubsection}{\nonumberline Umgekehrte Reihenfolge}{27}{section*.11}
\contentsline {subsection}{\numberline {3.3.2}Alle k-elementigen Teilmengen}{28}{subsection.3.3.2}
\contentsline {subsubsection}{\nonumberline Problem des minimalen Spannbaums}{28}{section*.13}
\contentsline {subsubsection}{\nonumberline Rucksack-Problem}{31}{section*.14}
\contentsline {subsection}{\numberline {3.3.3}Disjunkte Bl\IeC {\"o}cke}{31}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}\IeC {\"U}berlappende Bl\IeC {\"o}cke}{33}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Zuf\IeC {\"a}llige Bl\IeC {\"o}cke}{34}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Weitere Wahlm\IeC {\"o}glichkeiten}{35}{subsection.3.3.6}
\contentsline {section}{\numberline {3.4}Abbruchbedingungen - Theoretischer Anriss}{37}{section.3.4}
\contentsline {section}{\numberline {3.5}Kommentar zur Programmierung}{39}{section.3.5}
\contentsline {chapter}{\numberline {4}Ergebnisse und Auswertung}{40}{chapter.4}
\contentsline {section}{\numberline {4.1}Problem des minimalen Spannbaums}{41}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Alle k-elementigen Teilmengen bei gleicher Problemgr\IeC {\"o}\IeC {\ss }e}{41}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Alle k-elementige Teilmengen bei unterschiedlichen Problemgr\IeC {\"o}\IeC {\ss }en}{44}{subsection.4.1.2}
\contentsline {subsubsection}{\nonumberline Alle 2-elementigen Teilmengen}{44}{section*.21}
\contentsline {subsubsection}{\nonumberline Alle 3-elementigen Teilmengen}{45}{section*.24}
\contentsline {subsubsection}{\nonumberline Alle 4-elementigen Teilmengen}{47}{section*.27}
\contentsline {subsection}{\numberline {4.1.3}Vergleich verschiedener Blockwahlen}{49}{subsection.4.1.3}
\contentsline {subsubsection}{\nonumberline Vollst\IeC {\"a}ndige Graphen}{49}{section*.30}
\contentsline {subsubsection}{\nonumberline Karger Graph}{55}{section*.38}
\contentsline {section}{\numberline {4.2}Rucksack-Problem}{61}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Alle k-elementigen Teilmengen bei gleicher Problemgr\IeC {\"o}\IeC {\ss }e}{61}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Alle k-elementige Teilmengen bei unterschiedlichen Problemgr\IeC {\"o}\IeC {\ss }en}{63}{subsection.4.2.2}
\contentsline {subsubsection}{\nonumberline Alle 2-elementigen Teilmengen}{63}{section*.48}
\contentsline {subsubsection}{\nonumberline Alle 3-elementigen Teilmengen}{65}{section*.53}
\contentsline {subsubsection}{\nonumberline Alle 4-elementigen Teilmengen}{66}{section*.55}
\contentsline {subsection}{\numberline {4.2.3}Vergleich verschiedener Blockwahlen}{68}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Gemeinsamkeiten und Unterschiede}{75}{section.4.3}
\contentsline {chapter}{\numberline {5}Schluss}{77}{chapter.5}
