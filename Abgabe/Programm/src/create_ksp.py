import os.path as os
import random
import knapsack as ks


# create a random data set (weight, price of elements + capacity)
def create_random_data_set(elements, capacity):
    price = {}
    weight = {}

    for i in elements:
        price[i] = random.randint(1, int(capacity/2))
        weight[i] = random.randint(1, int(capacity/2))

    return price, weight


# generate random instances
def generate_random_problem(number_of_elements, capacity):
    elements = list(range(1, number_of_elements + 1))

    problem = ks.Knapsack()
    price, weight = create_random_data_set(elements, capacity)
    problem.initialize_data(elements, capacity, price, weight)

    return problem


# generate a list of elements
def list_of_elements(number_of_elements):
    return list(range(1, number_of_elements + 1))


# write file of instances
def write_problem_files(number_of_problems, number_of_elements, capacity):
    name = '../basis/ksp/{0}_{1}_info.dat'.format(number_of_elements, capacity)
    if os.exists(name):
        raise Exception('file already exists')

    # write info file
    file = open(name, 'w')
    file.write('# number of problems; {0}\n'.format(number_of_problems))
    file.write('# number of elements; {0}\n'.format(number_of_elements))
    file.write('# capacity; {0}\n\n'.format(capacity))
    file.write('# list of elements is not needed')
    file.close()

    # write data files
    elements = list(range(1, number_of_elements + 1))

    for i in range(number_of_problems):
        name = '../basis/ksp/{0}_{1}_{2}.dat'.format(number_of_elements, capacity, i)
        price, weight = create_random_data_set(elements, capacity)

        file = open(name, 'w')
        file.write('# index; price; weight')
        for j in elements:
            file.write('\n')
            file.write('{0}; {1}; {2}'.format(j, price[j], weight[j]))
        file.close()


# read file of instances
def read_problem_file(el, cap, number):
    elements = list(range(1, el + 1))

    # read problem file
    file = open('../basis/ksp/{0}_{1}_{2}.dat'.format(el, cap, number), 'r')
    file.readline()
    price = {}
    weight = {}
    for line in file.readlines():
        l_index, l_price, l_weight = line.split('; ')
        price[int(l_index)] = int(l_price)
        weight[int(l_index)] = int(l_weight)
    problem = ks.Knapsack()
    problem.initialize_data(elements, cap, price, weight)

    return problem
