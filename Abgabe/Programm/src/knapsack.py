from time import *
from gurobipy import *
from datetime import datetime as dt
from random import randint


class Knapsack(object):

    def __init__(self):
        self.filename = ''

        self.elements = []
        self.number_of_elements = 0
        self.price = {}
        self.weight = {}
        self.capacity = 0

        self.cur_optimum = 0
        self.cur_time = 0
        self.cur_status = ''
        self.cur_x = {}

    def initialize_data(self, elements, capacity, price, weight):
        self.elements = elements
        self.number_of_elements = len(elements)
        self.capacity = capacity
        self.price = price
        self.weight = weight

        self.start_allocation()

    # generate start allocation
    def start_allocation(self):
        weight = 0
        for i in self.elements:
            if weight + self.weight[i] <= self.capacity:
                self.cur_x[i] = 1
                weight += self.weight[i]
                self.cur_optimum += self.price[i]
            else:
                self.cur_x[i] = 0

    # generate list of elements for a specific block
    def block_elements(self, ids):
        start = ids[0]
        end = ids[1]

        if ids[1] <= self.number_of_elements:
            return list(range(start, end + 1))
        else:
            lst1 = list(range(start, self.number_of_elements + 1))
            lst2 = list(range(1, end % self.number_of_elements + 1))

            return lst1 + lst2

    # solve complete problem with gurobi
    def solve_complete(self):
        try:
            # create model
            prob = Model('ksp')
            prob.Params.OutputFlag = 0
            # create variables
            x = prob.addVars(self.elements, vtype=GRB.BINARY)
            # set objective
            expr = quicksum([self.price[e] * x[e] for e in self.elements])
            prob.setObjective(expr, GRB.MAXIMIZE)
            # add constraint
            prob.addConstr(quicksum([self.weight[i] * x[i] for i in x]), GRB.LESS_EQUAL, self.capacity, name='capacity constraint')

            # set start solution
            for i in self.elements:
                x[i].start = self.cur_x[i]

            t1 = clock()
            prob.optimize()
            t2 = clock()
            t = t2 - t1

            for e in self.elements:
                self.cur_x[e] = int(x[e].x)
            self.cur_optimum = prob.objVal
            self.cur_time = t
            self.cur_status = prob.status

        except GurobiError as e:
            print('Error {0}'.format(e))

        return self.cur_optimum

    # solve algorithm with block set
    def algorithm(self, blocks, block_info):
        try:
            '''Modelling'''
            # print('Start modelling')
            # model_s = dt.now()
            # create model
            prob = Model('ksp')
            prob.Params.OutputFlag = 0
            # create variables
            x = prob.addVars(self.elements, vtype=GRB.BINARY)
            # set objective
            expr = quicksum([self.price[e] * x[e] for e in self.elements])
            prob.setObjective(expr, GRB.MAXIMIZE)
            # add constraint
            prob.addConstr(quicksum([self.weight[i] * x[i] for i in x]), GRB.LESS_EQUAL, self.capacity, name='capacity constraint')

            # set start solution
            for e in self.elements:
                x[e].lb = self.cur_x[e]
                x[e].ub = self.cur_x[e]
            prob.update()
            prob.optimize()

            '''Algorithm'''
            counter = 1
            old_val = self.cur_optimum
            loop_val = [prob.objVal]

            time_start = dt.now()
            while True:
                for block in blocks:
                    # generate block list
                    if block_info[0].find('teilmengen') == -1:
                        lst = self.block_elements(block)
                    else:
                        lst = [i for i in block]

                    # set boundaries, e.g. free variables in lst, fix variables in lst_c
                    if block_info[0].find('teilmengen') == -1:
                        for e in self.elements:
                            if block[0] <= e <= block[1]:
                                x[e].lb = 0
                                x[e].ub = 1
                            elif block[1] >= self.number_of_elements and e <= (block[1] % self.number_of_elements):
                                x[e].lb = 0
                                x[e].ub = 1
                            else:
                                x[e].lb = int(x[e].x)
                                x[e].ub = int(x[e].x)
                    else:
                        for e in self.elements:
                            if e in lst:
                                x[e].lb = 0
                                x[e].ub = 1
                            else:
                                x[e].lb = int(x[e].x)
                                x[e].ub = int(x[e].x)

                    # update
                    prob.update()

                    # solve
                    prob.optimize()

                    for e in lst:
                        self.cur_x[e] = int(x[e].x)
                    self.cur_optimum = prob.objVal
                    self.cur_status = prob.status

                if old_val == prob.objVal:
                    break

                loop_val.append(prob.objVal)
                old_val = prob.objVal
                counter += 1

            time_end = dt.now()
            time_algo = (time_end - time_start).total_seconds()

            return counter, time_algo, loop_val

        except GurobiError as e:
            print('Error {0}'.format(e))

    # solve algorithm with random block set
    def algorithm_random(self, block_size):
        try:
            '''Modelling'''
            # print('Start modelling')
            # model_s = dt.now()
            # create model
            prob = Model('ksp')
            prob.Params.OutputFlag = 0
            # create variables
            x = prob.addVars(self.elements, vtype=GRB.BINARY)
            # set objective
            expr = quicksum([self.price[e] * x[e] for e in self.elements])
            prob.setObjective(expr, GRB.MAXIMIZE)
            # add constraint
            prob.addConstr(quicksum([self.weight[i] * x[i] for i in x]), GRB.LESS_EQUAL, self.capacity, name='capacity constraint')

            # set start solution
            for e in self.elements:
                x[e].lb = self.cur_x[e]
                x[e].ub = self.cur_x[e]
            prob.update()
            prob.optimize()

            '''Algorithm'''
            counter = 1
            old_val = self.cur_optimum
            loop_val = [prob.objVal]

            dict_used = {i: 0 for i in self.elements}
            check_used = False

            time_start = dt.now()
            while True:
                if counter%100 == 1:
                    print(counter)
                # generate random block list
                lst = []
                for i in range(block_size):
                    ri = randint(1, self.number_of_elements)
                    while ri in lst:
                        ri = randint(1, self.number_of_elements)

                    lst.append(ri)
                    dict_used[ri] += 1

                # set boundaries, e.g. free variables in lst, fix variables in lst_c
                for e in self.elements:
                    if e in lst:
                        x[e].lb = 0
                        x[e].ub = 1
                    else:
                        x[e].lb = int(x[e].x)
                        x[e].ub = int(x[e].x)

                # update
                prob.update()

                # solve
                prob.optimize()

                for e in lst:
                    self.cur_x[e] = int(x[e].x)

                self.cur_optimum = prob.objVal
                self.cur_status = prob.status

                # check if all elements are used
                if not check_used:
                    c = True
                    for t in dict_used.values():
                        if t == 0:
                            c = False
                            break
                    if c:
                        check_used = True

                if old_val == prob.objVal and check_used:
                    break

                loop_val.append(prob.objVal)
                old_val = prob.objVal
                counter += 1

            time_end = dt.now()
            time_algo = (time_end - time_start).total_seconds()

            return counter, time_algo, loop_val, dict_used

        except GurobiError as e:
            print('Error {0}'.format(e))
