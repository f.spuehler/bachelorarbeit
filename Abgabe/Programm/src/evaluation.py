from datetime import datetime as dt
import os.path as os
import create_blocks as cb


# start algorithm for one special block set (and write result files)
def calculating_one_block_set(problem_name, number_of_problems, problem_info, blocks, block_info):
    if problem_name == 'ksp':
        import create_ksp as cd
    elif problem_name == 'mst':
        import create_mst as cd
    else:
        raise Exception('problem_name = ksp or mst')

    print('Start {0} {1}_{2} with block {3} with {4} blocks'.format(problem_name, problem_info[0], problem_info[1], block_info[0], block_info[1]))

    res_opt, aver = read_optimal_solutions(problem_name, problem_info)
    loop_values = {}
    res_sol = []
    res_loops = []
    res_gap = []
    res_time = []
    res_freq = number_of_problems * [0]

    for i in range(number_of_problems):
        print('Solve problem', i)
        print(dt.now().strftime("%d.%m.%Y %H:%M:%S"))
        problem = cd.read_problem_file(problem_info[0], problem_info[1], i)
        
        loops, time, loop_val = problem.algorithm(blocks, block_info)
        loop_values[i] = loop_val
        sol = problem.cur_optimum
        gap = abs(sol-res_opt[i])/res_opt[i]

        res_sol.append(sol)
        res_loops.append(loops)
        res_gap.append(gap)
        res_time.append(time)
        if sol == res_opt[i]:
            res_freq[i] = 1
        
        # clear ram
        del problem

    # calculate average of all problems
    res_sol = sum(res_sol) / number_of_problems
    res_loops = sum(res_loops) / number_of_problems
    res_gap = sum(res_gap) / number_of_problems
    res_time = sum(res_time) / number_of_problems
    res_freq = sum(res_freq) / number_of_problems

    # print
    print('sol', 'loops', 'gap', 'time')
    print(res_sol, res_loops, res_gap, res_time)

    # write file
    file = open('../sol/{0}/{1}_{2}_{3}.eva'.format(problem_name, problem_info[0], problem_info[1], block_info[0]), 'w')
    # write information
    file.write('# block name; {0}\n'.format(block_info[0]))
    file.write('# number of blocks; {0}\n'.format(block_info[1]))
    file.write('#\n')

    if problem_name == 'ksp':
        file.write('# elements; {0}\n'.format(problem_info[0]))
        file.write('# capacity; {0}\n'.format(problem_info[1]))
    else:
        file.write('#nodes; {0}\n'.format(problem_info[0]))
        file.write('#edges; {0}\n'.format(problem_info[1]))

    file.write('#\n')
    file.write('# number of problems; {0}\n'.format(number_of_problems))
    file.write('# type; file\n')
    file.write('#\n')

    # write average values
    file.write('# rel. freq.; {0}\n'.format(res_freq))
    file.write('#sol; loops; gap; time\n')
    file.write('{0}; {1}; {2}; {3}\n'.format(res_sol, res_loops, res_gap, res_time))

    # write loop values
    file.write('#\n')
    file.write('# loop values')
    for i in range(number_of_problems):
        file.write('\n# problem {0}'.format(i))

        line = ''
        for val in loop_values[i]:
            line += str(val) + '; '
        line = line[:-2]
        file.write('\n{0}'.format(line))

    file.close()


# start algorihm for all k-subsets
def calculating_all_k_subsets(problem_name, number_of_problems, problem_info):
    if problem_name == 'ksp':
        import create_ksp as cd
        number_of_elements = len(cd.list_of_elements(problem_info[0]))
    else:
        import create_mst as cd
        number_of_elements = len(cd.list_of_elements(problem_info[0], problem_info[1]))

    dict_subsets = range(1, number_of_elements + 1)

    res_sol = {}
    res_loops = {}
    res_gap = {}
    res_time = {}
    res_abs = {}
    res_rel = {}

    # start solving
    time_start = dt.now().strftime("%d.%m.%Y %H:%M:%S")
    for k in dict_subsets:
        print('Start k-element-subset with k=', k)
        res_sol[k] = []
        res_time[k] = []
        res_loops[k] = []

        blocks, block_info = cb.teilmengen(number_of_elements, k)

        for i in range(number_of_problems):
            print('Solve problem', i)
            print(dt.now().strftime("%d.%m.%Y %H:%M:%S"))
            problem = cd.read_problem_file(problem_info[0], problem_info[1], i)

            loops, time, loop_val = problem.algorithm(blocks, block_info)

            res_sol[k].append(problem.cur_optimum)
            res_loops[k].append(loops)
            res_time[k].append(time)

            del problem

    # calculate gap and frequency
    for k in dict_subsets:
        res_gap[k] = []
        res_abs[k] = 0
        for i in range(number_of_problems):
            gap = abs(res_sol[k][i] - res_sol[number_of_elements][i]) / res_sol[number_of_elements][i]
            res_gap[k].append(gap)

            if res_sol[k][i] == res_sol[number_of_elements][i]:
                res_abs[k] += 1

    for key, val in res_abs.items():
        res_rel[key] = val / number_of_problems

    # calculate average of all problems
    for k in dict_subsets:
        res_sol[k] = sum(res_sol[k]) / number_of_problems
        res_loops[k] = sum(res_loops[k]) / number_of_problems
        res_gap[k] = sum(res_gap[k]) / number_of_problems
        res_time[k] = sum(res_time[k]) / number_of_problems

    res_freq = [sum(res_abs.values()) / number_of_elements, sum(res_rel.values()) / number_of_elements]

    # write file
    file = open('../sol/{0}/{1}_{2}_all-k-subs.eva'.format(problem_name, problem_info[0], problem_info[1]), 'w')
    file.write('#all k-element-subsets\n')
    file.write('#\n')

    if problem_name == 'ksp':
        file.write('# elements; {0}\n'.format(problem_info[0]))
        file.write('# capacity; {0}\n'.format(problem_info[1]))
    else:
        file.write('# nodes; {0}\n'.format(problem_info[0]))
        file.write('# edges; {0}\n'.format(problem_info[1]))

    file.write('#\n')
    file.write('# problems; {0}\n'.format(number_of_problems))
    file.write('# type; file\n')
    file.write('#\n')
    file.write('# abs. freq.; {0}\n'.format(res_freq[0]))
    file.write('# rel. freq.; {0}\n'.format(res_freq[1]))
    file.write('#\n')
    file.write('# k; sol; loops; gap; time; rel. freq.')
    for k in dict_subsets:
        file.write('\n')
        file.write('{0}; {1}; {2}; {3}; {4}; {5}'.format(k, res_sol[k], res_loops[k], res_gap[k], res_time[k], res_rel[k]))
    file.close()

    print('Start time:', time_start)
    print('End time:  ', dt.now().strftime("%d.%m.%Y %H:%M:%S"))


# start algorithm for a special k-subset for different sizes of instances
def calculating_k_el_subsets(problem_name, number_of_problems, k, start_size, end_size, step_size, complete_graph=False):
    if problem_name == 'ksp':
        import create_ksp as cd
    elif problem_name == 'mst':
        import create_mst as cd
    else:
        raise Exception('problem_name = ksp or mst')

    # results
    res_sol = {}
    res_loops = {}
    res_gap = {}
    res_time = {}
    res_freq = {}

    '''loop - problem size'''
    for problem_size in range(start_size, end_size+1, step_size):
        print('problem size:', problem_size)

        # initialize lists for results
        res_sol[problem_size] = []
        res_loops[problem_size] = []
        res_gap[problem_size] = []
        res_time[problem_size] = []
        res_freq[problem_size] = 0
        opt_sol = []

        # generate random problem
        if problem_name == 'ksp':
            problem = cd.generate_random_problem(problem_size, 5 * problem_size)
            number_of_elements = problem_size
        else:
            number_of_edges = int(problem_size * (problem_size - 1) / 2)
            if not complete_graph:
                number_of_edges = int(number_of_edges * 2 / 3)

            problem = cd.generate_random_problem(problem_size, complete_graph)
            number_of_elements = number_of_edges

        # generate blocks
        blocks, block_info = cb.teilmengen(number_of_elements, k)

        '''loop number of problems'''
        for i in range(number_of_problems):
            print('problem nr.:', i)
            # initialize new data
            if not i == 0:
                if problem_name == 'ksp':
                    elements = problem.elements
                    price, weight = cd.create_random_data_set(elements, 5 * problem_size)
                    problem.initialize_data(elements, 5 * problem_size, price, weight)
                else:
                    edges = []
                    for key, val in problem.edge_index.items():
                        edge = [key, val[0], val[1], 0]
                        edges.append(edge)
                    edges = cd.create_random_data_set(edges, number_of_elements)
                    problem.initialize_data(problem_size, number_of_elements, edges)

            print(dt.now().strftime("%d.%m.%Y %H:%M:%S"))
            loops, time, loop_val = problem.algorithm(blocks, block_info)

            value_alg = problem.cur_optimum
            res_sol[problem_size].append(value_alg)
            res_loops[problem_size].append(loops)
            res_time[problem_size].append(time)

            # calculate optimal value
            problem.solve_complete()
            value_opt = problem.cur_optimum
            opt_sol.append(value_opt)

            # rest of information
            gap = abs(value_alg - value_opt) / value_opt
            res_gap[problem_size].append(gap)
            if value_opt == value_alg:
                res_freq[problem_size] += 1

        # calculate averages
        res_sol[problem_size] = sum(res_sol[problem_size]) / number_of_problems
        res_loops[problem_size] = sum(res_loops[problem_size]) / number_of_problems
        res_gap[problem_size] = sum(res_gap[problem_size]) / number_of_problems
        res_time[problem_size] = sum(res_time[problem_size]) / number_of_problems
        res_freq[problem_size] = res_freq[problem_size] / number_of_problems

    # write file
    if problem_name == 'ksp':
        file = open('../sol/ksp/teilmengen({0})_{1}_{2}_{3}.eva'.format(k, start_size, end_size, step_size), 'w')
    else:
        file = open('../sol/mst/teilmengen({0})_{1}_{2}_{3}_{4}.eva'.format(k, complete_graph, start_size, end_size, step_size), 'w')

    file.write('#{0}-element-subsets for different problem sizes\n'.format(k))
    file.write('#from size {0} to {1} with step size of {2}\n'.format(start_size, end_size, step_size))
    file.write('#\n')
    file.write('#problems; {0} \n'.format(number_of_problems))

    if problem_name == 'ksp':
        file.write('#type; random\n')
        file.write('#\n')
        file.write('#number of elements; problem_size\n')
        file.write('#capacity; 5 * problem_size\n')

    if problem_name == 'mst':
        if complete_graph:
            file.write('#type; random complete graph\n')
        else:
            file.write('#type; random not complete graph\n')
        file.write('#\n')
        file.write('#number of nodes; n := problem_size\n')
        if complete_graph:
            file.write('#number of edges; n*(n-1)/2\n')
        else:
            file.write('#number of edges; 2/3 * n*(n-1)/2\n')

    file.write('#\n')
    file.write('#problem size; sol; loops; gap; time; frequency\n')
    for problem_size in range(start_size, end_size + 1, step_size):
        file.write('{0}; {1}; {2}; {3}; {4}; {5}\n'.format(problem_size, res_sol[problem_size], res_loops[problem_size], res_gap[problem_size], res_time[problem_size], res_freq[problem_size]))
    file.close()


# start algorithm for random block sets
def calculating_random_blocks(problem_name, number_of_problems, problem_info, block_size):
    if problem_name == 'ksp':
        import create_ksp as cd
        number_of_elements = problem_info[0]
    elif problem_name == 'mst':
        import create_mst as cd
        number_of_elements = problem_info[1]
    else:
        raise Exception('problem_name = ksp or mst')

    print('Start {0} {1}_{2} with block size {3}'.format(problem_name, problem_info[0], problem_info[1], block_size))

    res_opt, aver = read_optimal_solutions(problem_name, problem_info)
    res_sol = []
    res_loops = []
    res_gap = []
    res_time = []
    res_freq = number_of_problems * [0]
    loop_values = {}
    res_used = {i: [] for i in range(1, number_of_elements + 1)}

    for i in range(number_of_problems):
        print('Solve problem', i)
        print(dt.now().strftime("%d.%m.%Y %H:%M:%S"))
        problem = cd.read_problem_file(problem_info[0], problem_info[1], i)

        loops, time, loop_val, dict_used = problem.algorithm_random(block_size)
        loop_values[i] = loop_val
        sol = problem.cur_optimum
        gap = abs(sol - res_opt[i]) / res_opt[i]

        res_sol.append(sol)
        res_loops.append(loops)
        res_gap.append(gap)
        res_time.append(time)
        if sol == res_opt[i]:
            res_freq[i] = 1

        for i in res_used.keys():
            res_used[i].append(dict_used[i])

        # clear ram
        del problem

    # calculate average of all problems
    res_sol = sum(res_sol) / number_of_problems
    res_loops = sum(res_loops) / number_of_problems
    res_gap = sum(res_gap) / number_of_problems
    res_time = sum(res_time) / number_of_problems
    res_freq = sum(res_freq) / number_of_problems

    for i, l in res_used.items():
        res_used[i] = sum(l) / number_of_problems

    # print
    print('sol', 'loops', 'gap', 'time')
    print(res_sol, res_loops, res_gap, res_time)

    # write file
    file = open('../sol/{0}/{1}_{2}_zufall({3}).eva'.format(problem_name, problem_info[0], problem_info[1], block_size), 'w')
    # write information
    file.write('# block name; random blocks with {0} elements\n'.format(block_size))
    file.write('# number of blocks; {0}\n'.format(res_loops))
    file.write('#\n')

    if problem_name == 'ksp':
        file.write('# elements; {0}\n'.format(problem_info[0]))
        file.write('# capacity; {0}\n'.format(problem_info[1]))
    else:
        file.write('#nodes; {0}\n'.format(problem_info[0]))
        file.write('#edges; {0}\n'.format(problem_info[1]))

    file.write('#\n')
    file.write('# number of problems; -1 \n')
    file.write('# type; file\n')
    file.write('#\n')

    # write average values
    file.write('# rel. freq.; {0}\n'.format(res_freq))
    file.write('#sol; loops; gap; time\n')
    file.write('{0}; {1}; {2}; {3}\n'.format(res_sol, res_loops, res_gap, res_time))

    # write loop values
    file.write('#\n')
    file.write('# loop values')
    for i in range(number_of_problems):
        file.write('\n# problem {0}'.format(i))

        line = ''
        for val in loop_values[i]:
            line += str(val) + '; '
        line = line[:-2]
        file.write('\n{0}'.format(line))

    file.write('\n\n# how often every element was used\n')
    file.write('#id; used')
    for i, l in res_used.items():
        file.write('\n{0}; {1}'.format(i, l))

    file.close()

    # add to block_list
    # block_info = ['zufall({0})'.format(block_size)]
    # cb.add_to_block_list(problem_name, problem_info, block_info)


# calculating optimal solution for instances
def calculating_optimal_solutions(problem_name, number_of_problems, problem_info):
    if problem_name == 'ksp':
        import create_ksp as cd
    elif problem_name == 'mst':
        import create_mst as cd
    else:
        raise Exception('problem_name = ksp or mst')

    print('Start calculating optimal solutions of {0}_{1}'.format(problem_info[0], problem_info[1]))
    res_sol = []

    for i in range(number_of_problems):
        print('Problem', i)
        problem = cd.read_problem_file(problem_info[0], problem_info[1], i)
        optimum = problem.solve_complete()
        res_sol.append(optimum)
        print(optimum)

    # write file
    if problem_name == 'ksp':
        name = '../basis/ksp/{0}_{1}_sol_opt.dat'.format(problem_info[0], problem_info[1])
    else:
        name = '../basis/mst/{0}_{1}_sol_opt.dat'.format(problem_info[0], problem_info[1])
    file = open(name, 'w')
    file.write('# optimal values for each problem\n')
    file.write('# problem nr.; solution')
    for i in range(number_of_problems):
        file.write('\n')
        file.write('{0}; {1}'.format(i, res_sol[i]))
    file.close()


# read the file of optimal solutions
def read_optimal_solutions(problem_name, problem_info):
    opt_sol = []

    if problem_name == 'ksp':
        file = open('../basis/ksp/{0}_{1}_sol_opt.dat'.format(problem_info[0], problem_info[1]), 'r')
    else:  # if problem_name == 'mst':
        file = open('../basis/mst/{0}_{1}_sol_opt.dat'.format(problem_info[0], problem_info[1]), 'r')

    file.readline()
    file.readline()
    for line in file.readlines():
        problem_nr, opt_val = line.split('; ')
        opt_sol.append(float(opt_val))
    file.close()

    aver = sum(opt_sol)/len(opt_sol)

    return opt_sol, aver


# check algorithm for a block set and a specific instance
def check_specific_problem(problem_name, problem_number, problem_info, blocks, block_info):
    if problem_name == 'ksp':
        import create_ksp as cd
    elif problem_name == 'mst':
        import create_mst as cd
    else:
        raise Exception('problem_name = ksp or mst')

    print('Start {0} {1}_{2} with block {3} with {4} blocks'.format(problem_name, problem_info[0], problem_info[1], block_info[0], block_info[1]))

    print('Solve problem', problem_number)
    print(dt.now().strftime("%d.%m.%Y %H:%M:%S"))
    problem = cd.read_problem_file(problem_info[0], problem_info[1], problem_number)

    loops, time, loop_val = problem.algorithm(blocks, block_info)
    sol = problem.cur_optimum

    # clear ram
    del problem

    # calculate average of all problems

    # print
    print('sol', 'loops', 'time')
    print(sol, loops, time)

    print('loop values')
    print(loop_val)


